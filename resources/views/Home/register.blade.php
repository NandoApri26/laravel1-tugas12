<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tugas HTML || Welcome</title>
</head>

<body>
    <h2>Buat Account Baru!</h2>

    <h4>Sign Up Form</h4>

    <form action="{{ url('/welcome') }}" method="POST" enctype="multipart/form-data">
        @csrf
        <label for="fname">First name:</label><br><br>
        <input type="text" id="fname" name="fname" placeholder="Input your first name"><br><br>

        <label for="lname">Last name:</label><br><br>
        <input type="text" id="lname" name="lname" placeholder="Input your Last name"><br><br>

        <p>Gander:</p>

        <input type="radio" id="male" name="jk" value="Male" checked>
        <label for="male">Male</label><br>

        <input type="radio" id="female" name="jk" value="Female">
        <label for="female">Female</label><br>

        <input type="radio" id="other" name="jk" value="Other">
        <label for="other">Other</label> <br><br>

        <label>Nationality:</label><br><br>
        <select name="Nationality id="">
            <option value=" Indonesia" name="Narionality">Indonesia</option>
            <option value="Malaysia" name="Narionality">Malaysia</option>
            <option value="Timor Leste" name="Narionality">Timor Leste</option>
            <option value="Singapura" name="Narionality">Singapura</option>
        </select><br><br>

        <label>Language Spoken:</label><br><br>
        <input type="checkbox" name="language" value="0">Bahasa Indonesia <br>
        <input type="checkbox" name="language" value="1">English <br>
        <input type="checkbox" name="language" value="2">Other

        <p>Bio:</p>
        <textarea name="bio" id="" cols="30" rows="10" placeholder="Silahkan di isi bionya"></textarea>
        <br>
            <button type="submit" class="btn btn-primary me-1 mb-1">Kirim</button>
    </form>
</body>

</html>
