<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('Home.register');
    }
    public function welcome(Request $request)
    {
        // dd($request->all());
        $fname = $request['fname'];
        $lname = $request['lname'];
        $jk = $request['jk'];
        $nationality = $request['Nationality'];
        $language = $request['language'];
        $bio = $request['bio'];

        return view('Home.welcome', compact('fname', 'lname', 'jk', 'nationality', 'language', 'bio'));
    }
}
